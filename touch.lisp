;;
;; touch.lisp - Touch a thing in the file system.
;;

(defpackage :touch
  (:documentation "Touch a thing in the file system.")
  (:use :cl :opsys)
  (:export
   #:touch
   ))
(in-package :touch)

(defun touch (pathname &key seconds nanoseconds)
  (let ((path (safe-namestring pathname))
	fd)
    (unwind-protect
      (progn
	(setf fd (posix-open
		  path
		  (logior +O_WRONLY+ +O_CREAT+ +O_NOCTTY+ +O_NONBLOCK+)
		  #o666))
	(set-file-time fd))
      (posix-close fd))))

;; EOF
